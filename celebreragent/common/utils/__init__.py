import base64
import commands
import json
import os
import socket

import psutil as psutil

from celebreragent.common import service

PATH_STACK = []


def detect_services():

    def _load_components_from_json(path):
        enabled_components = []
        with open(path) as json_file:
            json_data = json.load(json_file)
        for item in json_data.keys():
            if json_data[item]:
                enabled_components.append(item)
        return enabled_components

    def _module_location():
        return os.path.dirname(os.path.abspath(service.__file__))

    service_map = {}
    for startup_file in [some_file for some_file in os.listdir("/etc/init/")
                         if some_file.endswith(".conf")]:
        component = os.path.basename(startup_file).split("-")[0]
        if component in _load_components_from_json(
            '%s/../etc/supported_components.json' % _module_location()
        ):
            if component not in service_map.keys():
                service_map[component] = []
            service_map[component].append(
                service.Service(os.path.join("/etc/init", startup_file)))

    return service_map


def detect_service_statuses(service_map=None):

    if not service_map:
        service_map = detect_services()

    process_list = []
    services_status = {cmp: {svc: 0 for svc in services} for cmp, services in service_map.items()}

    for process in psutil.process_iter():
        under_coverage = 1
        try:
            pinfo = process.as_dict(attrs=['cmdline', 'name'])
            if pinfo not in process_list:
                process_list.append(pinfo)
                if 'coverage' in pinfo.get('name') or 'coverage' in pinfo.get('cmdline'):
                    under_coverage = 2
                for cmp, services in service_map.items():
                    for svc in services:
                        if svc in (pinfo.get('name') or pinfo.get('cmdline')):
                            services.remove(svc)
                            services_status[cmp].update({svc: under_coverage})
        except psutil.NoSuchProcess:
            pass

    return services_status


def prepare_data(data, method):
    if method == 'compress':
        return base64.b64encode(data.encode('zlib'))
    elif method == 'decompress':
        return base64.b64decode(data.decode('zlib'))


def combine(path):
    pushd(path)
    commands.getoutput('%s combine' % coverage_bin())
    popd()


def coverage_bin():
    for command in ['python-coverage', 'coverage']:
        if not commands.getstatusoutput(command)[0]:
            return command
        return None


def pushd(path):
    global PATH_STACK
    current_path = os.getcwd()

    PATH_STACK.append(current_path)
    os.chdir(path)


def popd():
    global PATH_STACK

    if len(PATH_STACK) > 0:
        os.chdir(PATH_STACK.pop())


def get_hostname():
    return str(socket.gethostname().split('.')[0])