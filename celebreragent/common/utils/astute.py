from __future__ import with_statement
import yaml
import os

from oslo_config import cfg

CONF = cfg.CONF

ASTUTE = {}

if os.path.exists(CONF.astute_location):
    with open(CONF.astute_location) as astute_yaml:
        ASTUTE = yaml.load(astute_yaml)