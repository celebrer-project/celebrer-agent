import oslo_messaging

from celebreragent.common.config import CONF


def call(route_key, method, **kwargs):
    transport = oslo_messaging.get_transport(CONF)
    client_target = oslo_messaging.target.Target(CONF.var_rpc_exchange,
                                                 route_key)
    client = oslo_messaging.rpc.RPCClient(transport, client_target,
                                          timeout=CONF.var_rpc_client_timeout)
    client.prepare(retry=CONF.var_rpc_client_retry)
    client.call({}, method, **kwargs)


def cast(route_key, method, **kwargs):
    transport = oslo_messaging.get_transport(CONF)
    client_target = oslo_messaging.target.Target(CONF.var_rpc_exchange,
                                                 route_key, fanout=True)
    client = oslo_messaging.rpc.RPCClient(transport, client_target,
                                          timeout=CONF.var_rpc_client_timeout)
    client.prepare(retry=CONF.var_rpc_client_retry)
    client.cast({}, method, **kwargs)