from oslo_config import cfg
from oslo_log import log as logging

from celebreragent import version
from celebreragent.common import utils

agent_opts = [
    cfg.StrOpt('agent-uuid', default=utils.get_hostname,
               help='Unique agent identification (using hostname by default)'),
    cfg.StrOpt('astute-location', default='/etc/astute.yaml',
               help='Location with astute file (using for detect primary'
                    'controller)'),
    cfg.StrOpt('var-rpc-exchange', default='celebrer',
               help='Exchange name in rabbitmq'),
    cfg.IntOpt('var-rpc-client-timeout', default=15,
               help='Timeout for send message with rpc client'),
    cfg.IntOpt('var-rpc-client-retry', default=5,
               help='The number of attempts to send message with rpc client'),


]

CONF = cfg.CONF
CONF.register_cli_opts(agent_opts)


def parse_args(args=None, usage=None, default_config_files=None):
    logging.register_options(CONF)
    logging.setup(CONF, 'celebreragent')
    CONF(args=args,
         project='celebreragent',
         version=version.version_string,
         usage=usage,
         default_config_files=default_config_files)
